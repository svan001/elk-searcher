(function () {
	
'use strict';

var appElkSearcher = angular.module('appElkSearcher', ['ui.bootstrap']);

appElkSearcher.controller('mainController', function($scope, $http){
	// <<<< INIT
	var ELK_URL = 'http://46.101.72.183';
	
	// >>> END INIT
	
	// Cherche sur ELK et map les message en retour 
	$scope.searchELK = function(val) {
		console.log('Recherche ' + val);
			
		return $http.get(ELK_URL + '/elasticsearch/_search?q='+val).then(
			function (response){
				return response.data.hits.hits;/*.map(
					function(hit){
						return hit._source.message;
					});*/
			}	
		);	
	}
	
});

// END

}());